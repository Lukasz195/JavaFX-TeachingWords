﻿KeyENG;ValuePL;NumberRepeat
zigzag;linia łamana;50
y-axis;os rzednych;50
x-axis;os odciętych;50
width;szerokość;50
wide;szeroki;50
whole;calośc;50
wavy line;linia falista;50
volume;objetość;50
vertical number line;os rzedna;50
vertical;pionowy;50
vertex;wierzchołek;50
triangle;trojkąt;50
trapezoid;trapez;50
trapezium;trapez;50
top;górny;50
tangent;styczna;50
take a root;wyciagnąć pierwiastek;50
surface area;obszar powierzchni;50
superscript;indeks górny;50
sum;suma;50
subtrahend;odjemnik;50
subtraction;odejmowanie;50
subtract;odejmować;50
subscript;indeks dolny;50
straight line;linia prosta;50
straight angle;kąt polpelny;50
square root;pierwiastek kwadratowy;50
square;kwadrat;50
spherical;kulisty;50
sphere;kula;50
solve an equation;rozwiązać równanie;50
solution;rozwiązanie;50
solid line;linia ciągła;50
sketch a graph;narysować wykres;50
side;bok;50
set of points;zbior punktów;50
semisphere;połkula;50
semicircle;połokrąg;50
segment perpendicular bisector;symetralna odcinka;50
segment;część;50
segment;odcinek;50
segment;kawałek;50
section;cześć;50
section;wycinek;50
section;odcinek;50
secant;sieczna;50
scalene;nierównoboczny;50
satisfy an equation;spelnić równanie;50
rounded;zaokrąglony;50
round;zaokraglić;50
root;pierwiastek;50
right angled triangle;trójkąt prostokątny;50
right angle;kat prosty;50
rhombus;romb;50
rhomboid;równoległobok;50
repeating decimal;ułamek dziesiętny okresowy;50
remainder;reszta;50
reflex angle;kąt wklęsły;50
reduce to lowest terms;skrócić ułamek;50
recurring decimal;ułamek dziesiętny okresowy;50
rectangular;prostokątny;50
rectangle coordinate system;prostokątny układ wspołrzędnych;50
rectangle;prostokąt;50
reciprocal;wielkość odwrotna;50
real number;liczba rzeczywista;50
rational number;liczba wymierna;50
raise a number to a power;podnosić liczbę do potegi;50
radius;promień;50
quotient;iloraz;50
quadrant;ćwiartka;50
pyramid;ostrosłup;50
proper fraction;ułamek właściwy;50
product;iloczyn;50
prime number;liczba pierwsza;50
power;potęga;50
positive number;liczba dodatnia;50
polygon;wielobok;50
pointed;ostro zakończony;50
pointed;spiczasty;50
point;punkt;50
plane;płaszczyzna;50
perpendicular lines;linie pionowe;50
perpendicular lines;linie prostopadłe;50
perpendicular;prostopadły;50
perpendicular;pionowy;50
perimeter;obwód;50
pentagonal;pięciokątny;50
pentagon;pięciokąt;50
parallelogram;równoległobok;50
parallel lines;linie rownoległe;50
parallel;rownoległy;50
oval;owalny;50
oval;owal;50
ordinal number;liczba porządkowa;50
ordered pair;para uporządkowana;50
operation;działanie;50
odd number;liczba nieparzysta;50
octagon;ośmiokąt;50
obtuse-angled triangle;trójkąt rozwartokątny;50
obtuse angle;kąt rozwarty;50
numerator;licznik;50
numeral;cyfra;50
number line;oś liczbowa;50
number;liczba;50
negative number;liczba ujemna;50
naught/nought;zero;50
natural number;liczba naturalna;50
multiply;mnożenie;50
multiplier;mnożnik;50
multiplication;mnożenie;50
multiplicand;mnożna;50
minuend;odjemna;50
line;linia prosta;50
length;długość;50
leg;przyprostokątna;50
isosceles triangle;trójkąt równoramienny;50
isosceles;równoramienny;50
intersecting lines;linie przecinające się;50
intersect;przecinać;50
intersect;krzyżować sie;50
interior region;obszar wewnętrzny;50
interior;część wewnetrzna;50
interior;wnętrze;50
integer;liczba całkowita;50
inscribe in;wpisać w;50
inequality;nierówność;50
hypotenuse;przeciwprostokątna;50
horizontal number line;oś odcietych;50
horizontal;poziomy;50
hexagonal;sześciokątny;50
hexagon;szesciokąt;50
hemispherical;połkulisty;50
hemisphere;połkula;50
height;wysokość;50
altitude;wysokość;50
half-plane;połpłaszczyzna;50
greatest common divisor;największy wspólny dzielnik;50
greatest common factor;największy wspólny dzielnik;50
full angle;kąt pełny;50
fraction;ułamek;50
formula;wzór;50
factorize;rozkładać na czynniki;50
factorial;silnia;50
factor;czynnik;50
face;ściana;50
extract a root;wyciągać pierwiastek;50
expanded notation;zapis w formie rozszerzonej;50
even numer;liczba parzysta;50
equilateral triangle;trójkąt równoboczny;50
equation;równanie;50
edge;krawędź;50
dotted line;linia kropkowana;50
divisor;dzielnik;50
division;dzielenie;50
dividend;dzielna;50
divide;dzielić;50
displace;przesuwać;50
displace;przenosić;50
digit;cyfra;50
difference;rożnica;50
diameter;średnica;50
diagonal;przekątna;50
diagonal;linia ukośna;50
derivative;pochodna;50
depth;głębokość;50
denominator;mianownik;50
decimal fraction;ułamek dziesiętny;50
cylindrical;cylindryczny;50
cylindrical;walcowaty;50
cylinder;walec;50
curved line;linia krzywa;50
curve;krzywa;50
cubical;sześcienny;50
cube;sześcian;50
counter-clockwise;przeciwny do ruchu wskazowek zegara;50
anti-clockwise;przeciwny do ruchu wskazowek zegara;50
cord;cięciwa;50
convex;wypukły;50
congruent;przystający;50
cone;stożek;50
concavity;wklęsłość;50
concave;wklęsły;50
common logarithm;logarytm zwykly;50
common logarithm;logarytm dziesiętny;50
common fraction;ułamek zwykły;50
clockwise;zgodny z ruchem wskazówek zegara;50
circumscribe about;opisać na;50
circumference;obwód koła;50
circumcircle;okrąg opisany;50
circular;okrągły;50
circular;kolisty;50
circle;okrąg;50
circle;koło;50
chord;cięciwa;50
centre;środek;50
broken line;linia przerywana;50
bracket;nawias;50
bottom;dolny;50
block;bryła;50
bisectrix;symetralna;50
bisector;dwusieczna;50
bisect;dzielic na pół;50
bisect;przecinać;50
base-ten system;system dziesiątkowy;50
base;baza;50
base;podstawa;50
area;powierzchnia;50
area;obszar;50
arc;łuk;50
apex;wierzchołek;50
angle;kąt;50
adjacent;przyległy;50
addition;dodawanie;50
addend;składnik sumy;50
add;dodawać;50
acute angle;kąt ostry;50
a third;jedna trzecia;50
a half;połowa;50
the least common denominator;najmniejszy wspólny mianownik;50
