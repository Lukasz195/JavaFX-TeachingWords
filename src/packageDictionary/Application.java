package packageDictionary;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import MyException.BadHeaderInDictionary;
import packageDictionary.Dictionary;

public class Application {

	private Dictionary dictionary;
	private Library library;

	public Application(Library library, String file, String mode) throws FileNotFoundException, BadHeaderInDictionary,NoSuchElementException {

		this.library = library;
		dictionary = new Dictionary(createPathToFileDictionary(file), mode);
	}

	private Integer findFileInFolder(List<String> list, String file) {
		Integer result = null;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).compareTo(file) == 0)
				result = i;
		}
		return result;
	}

	private String createPathToFileDictionary(String file) {
		List<String> list = library.getFilslist();
		Integer index = this.findFileInFolder(list, file);
		StringBuilder sb = new StringBuilder();
		sb.append((library.getFullFolderPath() + "\\" + list.get(index)));
		return sb.toString();
	}

	public void showListFile() {
		StringBuilder sb = new StringBuilder();
		sb.append("Lista nazw plik�w s�ownikowych: ");
		List<String> list = library.getFilslist();
		for (int i = 0; i < list.size(); i++) {
			sb.append(list.get(i) + " ");
		}
		System.out.println(sb.toString());
	}

	public void save() throws FileNotFoundException, UnsupportedEncodingException {
		this.dictionary.save();
	}

	public Word RandWord() {
		return this.dictionary.randKey();
	}

	public void goodAnswer(Word key, String answer) {
		this.dictionary.decreaseNumberRepeat(key, answer);
	}

	public void badAnswer(Word key, String answer) {
		this.dictionary.increaseNumberRepeat(key, answer);
	}

	public Word checkStringInDictionary(LinkedList<Word> list, String key) {
		return this.dictionary.findWordInList(list, key);
	}

	public LinkedList<Word> getListFromDictionary(Word key) {
		return dictionary.getList(key);
	}
}
