package packageDictionary;

public class Word implements Comparable<Word> {
	private String Word;
	private Byte numberRepeat;

	Word(String Word, Byte numberRepeat) {
		this.Word = Word;
		this.numberRepeat = numberRepeat;
	}

	Word(Word a) {
		this.Word = a.getString();
		this.numberRepeat = a.getNumberRepeat();
	}

	public String getString() {

		return this.Word;
	}

	public String getKeyValue() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getString() + this.getKeyValue());
		return sb.toString();
	}

	public Byte getNumberRepeat() {
		return this.numberRepeat;
	}

	public void setNumberRepeat(Byte number) {
		this.numberRepeat = number;
	}

	public void decreaseNumberRepeat() {
		if (this.numberRepeat - 1 >= 0) {
			this.numberRepeat--;
		}
	}

	public void increaseNumberRepeat() {
		if (this.getNumberRepeat() < 50)
			this.numberRepeat++;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{ Tekst: |" + this.getString() + "| ; NumberRepeat: [" + this.getNumberRepeat() + "] }");
		return sb.toString();
	}

	public int compareTo(Word o) {
		String sThis = ((char) (this.getNumberRepeat() + 65)) + ' ' + this.getString();
		String sOther = ((char) (o.getNumberRepeat() + 65)) + ' ' + o.getString();
		return (sThis.compareTo(sOther)) * (-1);
	}

}
