package packageDictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DictionaryFile {

	private String columns;
	private List<String> records;
	private String nameFile;
	private Scanner read;
	private File file;

	protected DictionaryFile(String NameFile) throws FileNotFoundException {
		createNameFile(NameFile);
		this.file = new File(this.nameFile);
		read = new Scanner(file, "UTF-8");
	}

	private void createNameFile(String NameFile) {
		StringBuilder nameFile = new StringBuilder();
		nameFile.append(NameFile);
		nameFile.append(".csv");
		this.nameFile = nameFile.toString();
	}

	protected String getFileName() {
		return this.nameFile;
	}

	protected File getFile() {
		return this.file;
	}

	protected Scanner getRead() {
		return this.read;
	}

	protected void write(Dictionary dictionary) throws FileNotFoundException, UnsupportedEncodingException {
		createHeaderToWrite();
		records = new ArrayList<String>();
		if (dictionary.getMode() == "ENGPL") {
			createDictionaryENGPLToWrite(dictionary);
		} else if (dictionary.getMode() == "PLENG") {
			createDictionaryPLENGToWrite(dictionary);
		}
		saveToFile();
	}

	private void createHeaderToWrite() {
		Charset ch = Charset.forName("UTF-8");
		columns = "KeyENG;ValuePL;NumberRepeat";
		columns = new String((columns).getBytes(ch));
	}

	private void createDictionaryENGPLToWrite(Dictionary dictionary) {
		StringBuilder sb = new StringBuilder();
		for (Word key : dictionary.getData().keySet()) {
			LinkedList<Word> value = dictionary.getData().get(key);

			for (int j = 0; j < value.size(); j++) {
				sb.append(key.getString() + ";" + value.get(j).getString() + ";" + value.get(j).getNumberRepeat());

				String line = new String((sb.toString()));
				records.add(line);
				sb.delete(0, sb.length());
			}
		}
	}

	private void createDictionaryPLENGToWrite(Dictionary dictionary) {
		StringBuilder sb = new StringBuilder();
		for (Word key : dictionary.getData().keySet()) {
			LinkedList<Word> value = dictionary.getData().get(key);
			for (int j = 0; j < value.size(); j++) {
				sb.append(value.get(j).getString() + ";" + key.getString() + ";" + value.get(j).getNumberRepeat());
				String line = new String((sb.toString()));
				records.add(line);
				sb.delete(0, sb.length());
			}
		}
	}

	private void saveToFile() throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter save = new PrintWriter(nameFile, "UTF-8");
		save.println(columns);
		for (int i = 0; i < records.size(); i++) {
			save.println(records.get(i));
		}
		save.close();
	}
}
