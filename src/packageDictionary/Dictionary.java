package packageDictionary;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.TreeMap;

import MyException.BadHeaderInDictionary;

public class Dictionary {

	private DictionaryFile dictionaryFile;
	private TreeMap<Word, LinkedList<Word>> data;
	private String mode;

	protected Dictionary(String fileName, String mode, Byte numberRepeat) throws FileNotFoundException, BadHeaderInDictionary, NoSuchElementException {
		if (numberRepeat >= 0 && numberRepeat <= 50) {
			this.dictionaryFile = new DictionaryFile(fileName);
			this.data = new TreeMap<>();
			this.mode = mode.toUpperCase();
			String correctHeader = "KeyENG;ValuePL;NumberRepeat;";
			String line = getDictionaryFile().getRead().nextLine();
			if (line.compareTo(correctHeader) == 0) {
				while (getDictionaryFile().getRead().hasNextLine()) {
					line = getDictionaryFile().getRead().nextLine();
					String[] elements = line.split(";");
					String engString = elements[0];
					String plString = elements[1];
					Word keyWord = setKeyWord(plString, engString, mode);
					if (keyWord == null) {
						createNewKey(plString, engString, mode, numberRepeat, keyWord);
					} else {
						createNewNodeInList(plString, plString, mode, numberRepeat, keyWord);
					}
				}
			} else {
				 
				getDictionaryFile().getRead().close();
				throw new BadHeaderInDictionary(correctHeader, line);
			}
		}
		getDictionaryFile().getRead().close();
	}

	protected Dictionary(String fileName, String mode) throws FileNotFoundException, BadHeaderInDictionary,NoSuchElementException  {
		this.dictionaryFile = new DictionaryFile(fileName);
		this.data = new TreeMap<>();
		this.mode = mode.toUpperCase();
		String correctHeader = "KeyENG;ValuePL;NumberRepeat";
		String line = getDictionaryFile().getRead().nextLine();
		if (line.compareTo(correctHeader) == 0) {
			Word keyWord = null;
			while (getDictionaryFile().getRead().hasNextLine()) {
				line = getDictionaryFile().getRead().nextLine();
				String[] elements = line.split(";");
				String engString = elements[0];
				String plString = elements[1];
				String lastString = elements[elements.length - 1];
				Byte number = setNumberRepeat(lastString, plString);
				keyWord = setKeyWord(plString, engString, mode);
				if (keyWord == null) {
					createNewKey(plString, engString, mode, number, keyWord);
				} else {
					createNewNodeInList(plString, engString, mode, number, keyWord);
				}
			}
		} else {
			getDictionaryFile().getRead().close();
			throw new BadHeaderInDictionary(correctHeader, line);
		}
		getDictionaryFile().getRead().close();
	}

	private void createNewNodeInList(String plString, String engString, String mode, Byte number, Word keyWord) {
		Word engWord = new Word(engString, number);
		Word plWord = new Word(plString, number);
		LinkedList<Word> Listvalue = data.get(keyWord);
		data.remove(keyWord);
		if (mode == "ENGPL") {
			Listvalue.add(plWord);
			if (keyWord.getNumberRepeat().compareTo(plWord.getNumberRepeat()) < 0) {
				engWord.setNumberRepeat(plWord.getNumberRepeat());
			}
			this.putToTreeMap(engWord, Listvalue);
		} else if (mode == "PLENG") {
			Listvalue.add(engWord);
			if (keyWord.getNumberRepeat().compareTo(engWord.getNumberRepeat()) < 0) {
				plWord.setNumberRepeat(engWord.getNumberRepeat());
			}
			this.putToTreeMap(plWord, Listvalue);
		}
	}

	private void createNewKey(String plString, String engString, String mode, Byte number, Word keyWord) {
		LinkedList<Word> Listvalue = new LinkedList<>();
		if (mode == "ENGPL") {
			Word plWord = new Word(plString, number);
			Listvalue.add(plWord);
			keyWord = new Word(engString, number);
		} else if (mode == "PLENG") {
			Listvalue.add(new Word(engString, number));
			keyWord = new Word(plString, number);
		}
		data.put(keyWord, Listvalue);
	}

	private Word setKeyWord(String plString, String engString, String mode) {
		if (mode == "ENGPL") {
			return checkKey(engString);
		} else if (mode == "PLENG") {
			return checkKey(plString);
		}
		return null;
	}

	private Byte setNumberRepeat(String lastString, String plString) {
		if (lastString == plString) {
			return new Byte((byte) 50);
		} else {
			return new Byte(lastString);
		}
	}

	protected TreeMap<Word, LinkedList<Word>> getData() {
		return data;
	}

	protected String getMode() {
		return mode;
	}

	protected DictionaryFile getDictionaryFile() {
		return dictionaryFile;
	}

	protected Integer getSizeDictionary() {
		return data.size();
	}

	protected Word randKey() {

		LinkedList<Word> list = this.getListwithMaximatelyWords();
		Random rand = new Random();
		Word result = list.get(rand.nextInt(list.size()));
		return result;

	}

	private LinkedList<Word> getListwithMaximatelyWords() {
		LinkedList<Word> list = new LinkedList<>();
		Byte Max = data.firstKey().getNumberRepeat();
		for (Word key : data.keySet()) {
			if (key.getNumberRepeat().compareTo(Max) == 0)
				list.add(key);
		}
		return list;
	}

	protected Word checkKey(String key) {
		Word tmp = null;
		for (Byte i = 50; i >= 0; i--) {
			tmp = new Word(key, i);
			if (this.getData().containsKey(tmp) == true) {
				return tmp;
			}
		}
		tmp = null;
		return tmp;
	}

	protected Word findWordInList(LinkedList<Word> list, String key) {
		for (int i = 0; i < list.size(); i++) {
			Word tmp = list.get(i);
			if (tmp.getString().compareTo(key) == 0) {
				return tmp;
			}
		}
		return null;
	}

	protected Word getWordFromList(LinkedList<Word> list, Word key) {
		for (int i = 0; i < list.size(); i++) {
			Word tmp = list.get(i);
			if (tmp.compareTo(key) == 0) {
				return tmp;
			}
		}
		return null;
	}

	protected Word getWordFromList(LinkedList<Word> list, Integer key) {
		Word tmp = null;
		tmp = list.get(key);

		return tmp;
	}

	protected Word removeWordFromList(LinkedList<Word> list, String key) {
		Word tmp = null;
		for (int i = 0; i < list.size(); i++) {
			tmp = this.getWordFromList(list, i);
			if (tmp.getString().compareTo(key) == 0) {
				tmp = list.remove(i);
				break;
			}
		}
		return tmp;
	}

	public LinkedList<Word> getList(Word key) {

		return this.data.get(key);

	}

	protected LinkedList<Word> removeFromTreeMap(Word key) {
		LinkedList<Word> result = null;

		if (this.getData().containsKey(key) && key.getNumberRepeat() != 0) {
			result = this.getData().remove(key);
		}

		return result;
	}

	protected void addToList(LinkedList<Word> list, Word added) {
		list.add(added);
	}

	protected void putToTreeMap(Word key, LinkedList<Word> value) {
		this.getData().put(key, value);
	}

	protected void increaseNumberRepeat(Word key, String stringKey) {
		LinkedList<Word> listValue = this.removeFromTreeMap(key);
		Word findedWord = this.removeWordFromList(listValue, stringKey);
		findedWord.increaseNumberRepeat();

		Byte max = this.findMaxInList(listValue);
		if (key.getNumberRepeat() != max) {

			Word newKey = new Word(key);
			newKey.setNumberRepeat(max);
			this.putToTreeMap(newKey, listValue);

		} else {
			this.putToTreeMap(key, listValue);

		}

	}

	protected Word TestincreaseNumberRepeat(Word key, String stringKey) {
		LinkedList<Word> listValue = this.removeFromTreeMap(key);
		Word findedWord = this.removeWordFromList(listValue, stringKey);
		findedWord.increaseNumberRepeat();
		this.addToList(listValue, findedWord);
		Byte max = this.findMaxInList(listValue);
		if (key.getNumberRepeat() != max) {
			Word newKey = new Word(key);
			newKey.setNumberRepeat(max);
			this.removeFromTreeMap(newKey);
			this.putToTreeMap(newKey, listValue);
			return newKey;
		} else {
			this.putToTreeMap(key, listValue);
			return key;
		}
	}

	protected Byte findMaxInList(LinkedList<Word> list) {
		Byte max = list.get(0).getNumberRepeat();
		for (int i = 0; i < list.size(); i++) {
			if (max < list.get(i).getNumberRepeat()) {
				max = list.get(i).getNumberRepeat();
			}
		}
		return max;
	}

	protected void decreaseNumberRepeat(Word key, String stringKey) {
		LinkedList<Word> listValue = this.removeFromTreeMap(key);
		Word findedWord = this.removeWordFromList(listValue, stringKey);
		findedWord.decreaseNumberRepeat();
		this.addToList(listValue, findedWord);
		Byte max = this.findMaxInList(listValue);
		if (key.getNumberRepeat() != max) {
			Word newKey = new Word(key);
			newKey.setNumberRepeat(max);
			this.putToTreeMap(newKey, listValue);
		} else {
			this.putToTreeMap(key, listValue);
		}
	}

	protected Word TestdecreaseNumberRepeat(Word key, String stringKey) {
		if (key.getNumberRepeat() > 0) {
			LinkedList<Word> listValue = this.removeFromTreeMap(key);
			// this.show();
			// System.out.println(listValue);
			Word findedWord = this.removeWordFromList(listValue, stringKey);
			findedWord.decreaseNumberRepeat();
			this.addToList(listValue, findedWord);
			// System.out.println(listValue);

			Byte max = this.findMaxInList(listValue);
			if (key.getNumberRepeat() != max && key.getNumberRepeat() > 0) {
				Word newKey = new Word(key);
				newKey.setNumberRepeat(max);
				this.removeFromTreeMap(newKey);
				this.putToTreeMap(newKey, listValue);
				// this.show();
				return newKey;
			} else {
				this.putToTreeMap(key, listValue);
				// this.show();
				return key;
			}
		}
		return key;
	}

	protected Integer RandMemberFromList(LinkedList<Word> list) {
		Random rand = new Random();
		Integer index = rand.nextInt(list.size());
		return index;
	}

	protected boolean TestdecreaseDictionary(Integer how) {
		Boolean result = true;
		try {
			for (int i = 0; i < how; i++) {
				Word selectedWord = this.randKey();
				LinkedList<Word> list = this.getList(selectedWord);
				Integer index = this.RandMemberFromList(list);
				String selectedString = this.getWordFromList(list, index).getString();
				selectedWord = this.TestdecreaseNumberRepeat(selectedWord, selectedString);
			}
		} catch (NullPointerException e) {
			result = false;
		}
		return result;
	}

	protected boolean TestincreaseDictionary(Integer how) {
		Boolean result = true;
		try {
			for (int i = 0; i < how; i++) {
				Word selectedWord = this.randKey();
				LinkedList<Word> list = this.getData().get(selectedWord);
				Random rand = new Random();
				Integer index = rand.nextInt(list.size());
				String selectedString = list.get(index).getString();
				selectedWord = this.TestincreaseNumberRepeat(selectedWord, selectedString);
			}
		} catch (NullPointerException e) {
			result = false;
		}
		return result;
	}

	protected void save() throws FileNotFoundException, UnsupportedEncodingException {
		dictionaryFile.write(this);
	}

	protected void show() {
		System.out.println("\nWidok pokazowy - rozmiar TreeMap : " + data.size());
		StringBuilder sb = new StringBuilder();
		int iterator = 1;
		for (Word key : data.keySet()) {
			LinkedList<Word> value = data.get(key);
			System.out.print(iterator + "." + " { " + key.getString() + " [ " + key.getNumberRepeat() + " ] } -> | ");
			if (value != null) {
				for (int i = 0; i < value.size(); i++) {
					Word tmp = value.get(i);
					sb.append(tmp.getString() + "{ " + tmp.getNumberRepeat() + " } | ");
				}
			}
			System.out.println(sb.toString());
			sb.delete(0, sb.length());
			iterator++;
		}
		System.out.println();
	}

	protected void test() {
		System.out.println("Widok pokazowy - rozmiar TreeMap : " + data.size() + "\n");
		int i = 1;
		for (Word key : data.keySet()) {
			System.out.println(i + ". Klucz -> " + key.toString());
			System.out.println("Wartosc pod kluczem -> " + data.get(key));
			System.out.println();
			i++;
		}
	}

}
