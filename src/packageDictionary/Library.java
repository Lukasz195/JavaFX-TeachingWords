package packageDictionary;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import MyException.NotExistDictionarysFolder;

public class Library {
	List<String> listDictionaryFiles;
	String fullFolderPath;
	String nameFolder;
	File directory;

	public Library() throws NotExistDictionarysFolder {
		setPathApplication();
		this.nameFolder = "Dictionarys";
		directory = new File(fullFolderPath);
		if (!directory.isDirectory()) {
			throw new NotExistDictionarysFolder(nameFolder);
		} else {
			File[] files = directory.listFiles();
			for (File file : files) {
				if (file.isFile()) {
					String name = file.getName();
					String[] nameS = name.split(".csv");
					this.listDictionaryFiles.add(nameS[0]);
				}
			}
		}
	}

	private void setPathApplication() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.getProperty("user.dir") + "\\Dictionarys");
		listDictionaryFiles = new ArrayList<String>();
		fullFolderPath = sb.toString();
	}
	
	public List<String> getFilslist() {
		return this.listDictionaryFiles;
	}

	public String getFullFolderPath() {
		return this.fullFolderPath;
	}

	public String toString() {
		System.out.print("{ " + this.nameFolder + " } -> { ");
		StringBuilder sb = new StringBuilder();
		for (String name : this.listDictionaryFiles) {
			sb.append(" " + name + " ");
		}
		sb.append(" }");
		return sb.toString();
	}

}
