package application;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;

import MyException.BadHeaderInDictionary;
import MyException.NotExistDictionarysFolder;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import packageDictionary.Application;
import packageDictionary.Library;
import packageDictionary.Word;

public class Controller implements Initializable {

	@FXML
	private BorderPane root;

	@FXML
	private MenuBar mainMenuBar;

	@FXML
	private Menu mainMenuDictionarys;

	@FXML
	private Menu mainMenuHelp;

	@FXML
	private Label mainLabel;

	@FXML
	private Label mainLabelResult;

	@FXML
	private TextField mainInputTextField;

	@FXML
	private Label mainLabelDescription;

	@FXML
	private Button mainCheckButton;

	private Library libraryFilesCSV;
	private Application app;
	private String textmainLabel;
	private Word word;
	private String currentDictionaryName;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainCheckButton.setDisable(true);
		mainInputTextField.setDisable(true);
		try {
			initMenuDictionarys();
		} catch (NotExistDictionarysFolder e) {
			NotExistDictionarysFolderHandling();
			e.printStackTrace();
		}
		initDictionary(libraryFilesCSV);

	}

	private void initMenuDictionarys() throws NotExistDictionarysFolder {
		libraryFilesCSV = new Library();
		List<String> list = libraryFilesCSV.getFilslist();
		Integer HowDictionarysInMenu = list.size();

		for (int i = 0; i < HowDictionarysInMenu; i++) {
			Menu next = new Menu(list.get(i));
			MenuItem ENGPL = new MenuItem("ENGPL");
			MenuItem PLENG = new MenuItem("PLENG");
			next.getItems().addAll(ENGPL, PLENG);
			mainMenuDictionarys.getItems().add(next);
		}
		mainLabel.setText("Welcome in Teaching LearningApp");
		mainLabelResult.setVisible(false);
		mainLabelDescription.setText("Choose the dictionary to learning");
	}

	private void initDictionary(Library libraryFilesCSV) {

		for (MenuItem item : mainMenuDictionarys.getItems()) {
			for (MenuItem subItem : ((Menu) item).getItems())
				subItem.setOnAction((ActionEvent) -> {
					setBlackColorLabels();
					clearLabels();
					try {
						if (app != null)
							app.save();
					} catch (FileNotFoundException e1) {
						FileNotFoundExceptionHandling();
						e1.printStackTrace();
					} catch (UnsupportedEncodingException e1) {
						UnsupportedEncodingExceptionHandling();
						e1.printStackTrace();
					}
					mainCheckButton.setDisable(false);
					mainInputTextField.setDisable(false);
					try {
						currentDictionaryName = item.getText();
						app = new Application(libraryFilesCSV, item.getText(), subItem.getText());
						textmainLabel = new String("You have chosen " + item.getText() + " " + subItem.getText());
						mainLabel.setText(textmainLabel);
					} catch (FileNotFoundException e) {
						app = null;
						FileNotFoundExceptionHandling();
						e.printStackTrace();
					}catch (NoSuchElementException e) {
						app = null;
						NoSuchElementExceptionHandling();
						e.printStackTrace();
					} catch (BadHeaderInDictionary e) {
						app = null;
						BadHeaderInDictionaryHandling(e);
						e.printStackTrace();
					}

					runApp();
				});
		}
	}

	private void runApp() {
		word = app.RandWord();
		mainLabel.setText(textmainLabel);
		mainLabelDescription.setText("Yours word it is : \n" + word.getString());
		mainInputTextField.clear();
	}

	@FXML
	private void checkPressEnter(KeyEvent e) {
		if (e.getCode().toString().equals("ENTER")) {
			checkOnClick();
		}
	}

	@FXML
	private void checkOnClick() {

		LinkedList<Word> list = app.getListFromDictionary(word);
		String userAnswer = mainInputTextField.getText();
		Word fooned = app.checkStringInDictionary(list, userAnswer);
		if (fooned == null) {
			mainLabelResult.setTextFill(new Color(1, 0, 0, 1));
			mainLabelResult.setVisible(true);
			mainLabelResult.setText("WRONG! +1");
			app.badAnswer(word, userAnswer);
			this.runApp();
			mainInputTextField.clear();

		} else {
			mainLabelResult.setTextFill(new Color(0, 1, 0, 1));
			mainLabelResult.setVisible(true);
			mainLabelResult.setText("EXCELLENT! -1");
			app.goodAnswer(word, userAnswer);
			this.runApp();
			mainInputTextField.clear();
		}
	}

	private void FileNotFoundExceptionHandling() {
		setRedColorLabels();
		mainLabel.setText("WARNING!");
		mainLabelDescription.setText("I don't find your file \\n Please check it location");
		mainLabelResult.setText("FIX IT FAST!");
	}

	private void BadHeaderInDictionaryHandling(BadHeaderInDictionary e) {

		mainLabel.setText("WARNING!");
		mainLabelDescription.setText("You set bad header in \"" + currentDictionaryName + "\" !\nYour header: \""
				+ e.getunCorrectHeader() + "\"\ncorrect header: \"" + e.getCorrectHeader() + "\"");
		mainLabelResult.setVisible(true);
		mainLabelResult.setText("FIX IT FAST!");
		setRedColorLabels();
	}

	private void UnsupportedEncodingExceptionHandling() {

		mainLabel.setText("WARNING!");
		mainLabelDescription.setText("You use in Dictionarys files foreign coding to me");
		mainLabelResult.setText("FIX IT FAST!");
		setRedColorLabels();
	}

	private void NotExistDictionarysFolderHandling() {

		mainLabel.setText("WARNING!");
		mainLabelDescription.setText(
				"In the foolowing path:\n" + System.getProperty("user.dir") + "\nI can't find Dictionarys folder!");
		mainLabelResult.setText("FIX IT FAST!");
		setRedColorLabels();
	}

	private void NoSuchElementExceptionHandling() {
		mainLabel.setText("WARNING!");
		mainLabelDescription.setText("You use \"" + currentDictionaryName + "\" !\nYour file is empty");
		mainLabelResult.setVisible(true);
		mainLabelResult.setText("FIX IT FAST!");
		setRedColorLabels();
	}

	private void setRedColorLabels() {
		mainLabel.setTextFill(new Color(1, 0, 0, 1));
		mainLabelDescription.setTextFill(new Color(1, 0, 0, 1));
		mainLabelResult.setTextFill(new Color(1, 0, 0, 1));
	}

	private void setBlackColorLabels() {
		mainLabel.setTextFill(new Color(0, 0, 0, 1));
		mainLabelDescription.setTextFill(new Color(0, 0, 0, 1));
		mainLabelResult.setVisible(false);
	}
	
	private void clearLabels() {
		mainLabel.setText("");
		mainLabelDescription.setText("");
		mainLabelResult.setText("");
	}
}
