package MyException;

public class WrongHeaderInFileException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WrongHeaderInFileException(String msg) {
		super(msg);
	}
}
