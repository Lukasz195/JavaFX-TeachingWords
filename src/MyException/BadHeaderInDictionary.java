package MyException;

import java.io.IOException;

public class BadHeaderInDictionary extends IOException {

	private static final long serialVersionUID = 1L;
	private String correctHeader;
	private String unCorrectHeader;

	public BadHeaderInDictionary() {
	}

	public BadHeaderInDictionary(String correctHeader, String unCorrectHeader) {
		super("GOOD { " + correctHeader + " } vs BAD {" + unCorrectHeader + " }");
		this.correctHeader = correctHeader;
		this.unCorrectHeader = unCorrectHeader;
	}

	public String getCorrectHeader() {
		return correctHeader;
	}

	public String getunCorrectHeader() {
		return unCorrectHeader;
	}

}
