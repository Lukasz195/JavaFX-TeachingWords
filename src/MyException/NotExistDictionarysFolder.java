package MyException;

import java.io.IOException;

public class NotExistDictionarysFolder extends IOException {

	private static final long serialVersionUID = 1L;
	private String NameFile;
	public NotExistDictionarysFolder() {}
	public NotExistDictionarysFolder(String gripe) {
		super(gripe);
		NameFile = gripe;
	}
	public String getNameFile() {
		return NameFile;
	}
	
}
